package com.bawei.common.mvvm.viewmodel;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

import com.bawei.common.bus.LiveDataBus;
import com.bawei.common.entity.BaseEntity;
import com.bawei.common.field.ActionEntity;
import com.bawei.common.field.UIBusAction;
import com.bawei.common.mvvm.model.BaseModel;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:43
 * Description: This is BaseViewModel
 */
public abstract class BaseViewModel<M extends BaseModel> extends ViewModel
implements IViewModel, Observer<BaseEntity> {
    protected M m;
    protected CompositeDisposable disposable;//收集全部rxjava订阅，防止内存泄露
    private UILiveDataBus uiLiveDataBus = new UILiveDataBus();

    public UILiveDataBus getUiLiveDataBus() {
        return uiLiveDataBus;
    }

    @Override
    public void create() {
        disposable = new CompositeDisposable();
        createModel();
        initData();
    }

    //创建对象
    private void createModel() {
        //反射获取泛型类型 创建对象
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        Class clazz = (Class)  parameterizedType.getActualTypeArguments()[0];
        try {
            m = (M) clazz.newInstance();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        disposable.dispose();
        disposable.clear();
    }

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        disposable.add(d);
        disposable.clear();
    }

    protected abstract void initData();

    @Override
    public void onNext(@NonNull BaseEntity baseEntity) {
        if (baseEntity.status != 200) {
            //showErroy
            showErroy(baseEntity.message);
            return;
        }
        result(baseEntity);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        showErroy("请求出错");
    }

    @Override
    public void onComplete() {
        disDialog();
    }

    protected abstract void result(BaseEntity entity);

    //启动界面->发送livedata事件
    protected void startPage(Class clazz) {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.STARTPAGE;
        Map<String,Object> map = new HashMap<>();
        map.put(ActionEntity.CLASSKEY,clazz);
        uiBusAction.parames = map;
        uiLiveDataBus.setDataBus(uiBusAction);
    }

    //启动界面-> 携带参数
    protected void startPage(Class clazz, Bundle bundle) {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.STARTPAGE;
        Map<String,Object> map = new HashMap<>();
        map.put(ActionEntity.CLASSKEY,clazz);
        map.put(ActionEntity.BUNDLEKEY,bundle);
        uiBusAction.parames = map;
        uiLiveDataBus.setDataBus(uiBusAction);
    }

    //关闭界面
    protected void finishPage() {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.FINISHPAGE;
        uiLiveDataBus.setDataBus(uiBusAction);
    }

    protected void showDialog() {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.SHOWDIALOG;
        uiLiveDataBus.setDataBus(uiBusAction);
    }

    protected void disDialog() {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.DISDIALOG;
        uiLiveDataBus.setDataBus(uiBusAction);
    }

    protected void showErroy(String msg) {
        UIBusAction uiBusAction = new UIBusAction();
        uiBusAction.action = ActionEntity.SHOWERRO;
        Map<String, Object> map = new HashMap<>();
        map.put(ActionEntity.ERROKEY, msg);
        uiBusAction.parames = map;
        uiLiveDataBus.setDataBus(uiBusAction);
    }



    //livedata 事件驱动创建
    public static class UILiveDataBus{
        private LiveDataBus<UIBusAction> uilidatabus = new LiveDataBus<>();
        public LiveDataBus<UIBusAction> getUilidatabus() {
            return uilidatabus;
        }
        public void setDataBus(UIBusAction action) {
            uilidatabus.setValue(action);
        }
    }
}

