package com.bawei.common.mvvm.model;

import com.bawei.common.entity.BaseEntity;
import com.bawei.common.entity.ErroyEntity;
import com.bawei.common.field.ErroyCode;
import com.bawei.common.field.ErroyMsg;
import com.bawei.common.network.RXFuntion;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:23
 * Description: This is BaseModel
 */
public abstract class BaseModel implements IModel{
    @Override
    public RequestBody createBody(Map<String, Object> map) {
        if (map == null) {
            return RequestBody.create(MediaType.parse("application/json"),"");
        }
        return RequestBody.create(MediaType.parse("application/json"),
                new Gson().toJson(map));
    }

    @Override
    public Observable<BaseEntity> changeObservable(Observable parames) {
        return parames.map(new RXFuntion());
    }

    @Override
    public Observable<BaseEntity> mergeObservable(Observable... parames) {
        ErroyEntity entity = new ErroyEntity();
        entity.status = ErroyCode.PARAMESCODE;
        entity.message = ErroyMsg.PARAMESMSG;
        if (parames == null) {
            return Observable.just(entity);
        }
        if (parames.length == 0) {
            return Observable.just(entity);
        }
        List<Observable<BaseEntity>> list = new ArrayList<>();
        for (int i = 0; i < parames.length; i++) {
            list.add(parames[i]);
        }
        return Observable.merge(list);
    }
}
