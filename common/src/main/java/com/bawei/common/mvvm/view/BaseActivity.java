package com.bawei.common.mvvm.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bawei.common.BaseApplication;
import com.bawei.common.field.ActionEntity;
import com.bawei.common.field.UIBusAction;
import com.bawei.common.mvvm.viewmodel.BaseViewModel;

import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:36
 * Description: This is BaseView
 */
public abstract class BaseActivity<V extends ViewDataBinding, VM extends BaseViewModel> extends AppCompatActivity implements IView {
    protected V v;
    protected VM vm;
    private UIObsever obsever;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //沉浸式状态栏导入
        createViewModel();
        obsever = new UIObsever(this);
        
        //databin创建
        v = DataBindingUtil.setContentView(this,bindLayout());
        v.setVariable(createvarableId(),vm);

        //将vm设置到databinding
        getLifecycle().addObserver(vm);
        vm.getUiLiveDataBus().getUilidatabus().observe(this,obsever);
    }
    
    protected abstract int createvarableId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vm.getUiLiveDataBus().getUilidatabus().removeObserver(obsever);
        getLifecycle().removeObserver(vm);
        v.unbind();
    }

    @Override
    public void createViewModel() {
        ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
        Class clazz = (Class) parameterizedType.getActualTypeArguments()[1];
        vm = (VM) new ViewModelProvider(this,
                new ViewModelProvider.NewInstanceFactory()).get(clazz);
    }

    @Override
    public void startPage(Class clazz) {
        startActivity(new Intent(this,clazz));
    }
    
    //Bundle->传参大小限制为1mb
    @Override
    public void startPage(Class clazz, Bundle bundle) {
        Intent intent = new Intent(this,clazz);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void finishPage() {
        finish();
    }

    @Override
    public void showDialog() {
        
    }

    @Override
    public void disDialog() {
        
    }

    @Override
    public void showErroy(String msg) {
        Toast.makeText(BaseApplication.getInstance(), msg, Toast.LENGTH_SHORT).show();
    }

    public static class UIObsever implements Observer<UIBusAction> {
        private WeakReference<BaseActivity> weakReference;
        
        public UIObsever(BaseActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onChanged(UIBusAction uiBusAction) {
            if (weakReference.get() == null) {
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.SHOWERRO)) {
                String str = (String) uiBusAction.parames.get(ActionEntity.ERROKEY);
                weakReference.get().showErroy(str);
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.STARTPAGE)) {
                Class clazz = (Class) uiBusAction.parames.get(ActionEntity.CLASSKEY);
                weakReference.get().startPage(clazz);
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.STARTBUNDLEPAGE)) {
                Class clazz = (Class) uiBusAction.parames.get(ActionEntity.CLASSKEY);
                Bundle bundle = (Bundle) uiBusAction.parames.get(ActionEntity.BUNDLEKEY);
                weakReference.get().startPage(clazz,bundle);
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.FINISHPAGE)) {
                weakReference.get().finishPage();
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.SHOWDIALOG)) {
                weakReference.get().showDialog();
                return;
            }
            if (uiBusAction.action.equals(ActionEntity.DISDIALOG)) {
                weakReference.get().disDialog();
                return;
            }
        }
    }
}
