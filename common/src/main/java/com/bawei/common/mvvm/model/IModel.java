package com.bawei.common.mvvm.model;

import com.bawei.common.entity.BaseEntity;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:09
 * Description: This is IModel
 */
public interface IModel {
    //创建requestbody
    RequestBody createBody(Map<String, Object> map);
    //转化被观察者任务
    Observable<BaseEntity> changeObservable(Observable parames);
    //合并被观察者任务
    Observable<BaseEntity> mergeObservable(Observable ... parames);
}
