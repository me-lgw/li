package com.bawei.common.mvvm.view;


import android.os.Bundle;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:09
 * Description: This is IModel
 */
public interface IView {
    //指定databinding要绑定的XML的id
    int bindLayout();
    //创建viewmodel
    void createViewModel();
    void startPage(Class clazz);
    void startPage(Class clazz, Bundle bundle);
    void finishPage();
    void showDialog();
    void disDialog();
    void showErroy(String msg);
}
