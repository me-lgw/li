package com.bawei.common.mvvm.viewmodel;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.bawei.common.entity.BaseEntity;

import java.util.Map;


/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 11:09
 * Description: This is IModel
 */
public interface IViewModel extends LifecycleObserver {
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    void create();

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    void destroy();

}
