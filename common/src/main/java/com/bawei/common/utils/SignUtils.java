package com.bawei.common.utils;

import com.blankj.utilcode.util.EncryptUtils;

import java.util.Arrays;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 15:49
 * Description: This is SignUtils
 */
public class SignUtils {
    /**
     * 全部参数值拼接为字符串str
     * str升序排序+tamboo
     * md5(str)
     * 转小写
     * */
    public static String createSign(String values) {
        //原始字符串拆分为char数组
        char[] chars = values.toCharArray();

        //升序排序
        Arrays.sort(chars);

        //获取排序后的字符串
        String str = new String(chars);
        str += "tamboo";
        return EncryptUtils.encryptMD5ToString(str).toLowerCase();
    }
}
