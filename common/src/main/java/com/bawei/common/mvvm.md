MVVM:Model层用来获取数据;
ViewModel层处理业务逻辑以及监听;
View层负责界面展示;
注意:ViewModel与Model层相互持有对象;View层持有ViewModel层对象但是ViewModel 不持有View对象
MVVM与MVP相比区别：MVVM是在MVP的基础上发展而来的，MVP 中V层与P层对象相互持有， MVVM中V层持有VM层对象但是VM不能持有V层对象；
在解耦性上MVVM更强->由于MVP中VP是耦合再一次，但是VM中没有耦合关系 View层更加纯粹只负责展示界面 由于每个接口请求参数不确定Map<String,Value> new Gson().tojson();
Observable<取消订单>观察者Obsever<取消订单> Observable<确认订单>观察者Obsever<确认订单> map()->转换操作符号->Observable ->观察者Obsever merge()->合并操作->合并4个被观察者->4个接口请求 onnext(){接收返回值BaseEntity instancof 确认当前类类型} oncomplate()->全部任务完成进入
MVVM如何架构:IModel:处理数据获取:createRequestBody(map), Observable map(Observable) Observable merge(Observable ... parames);
JD首页4个接口->进入界面发起网络请求->显示加载进度 4个接口全部请求完成（无论结果）->取消加载进度条
IViewModel:继承lifecycle 绑定生命周期create();
destrory()rxjava任务解除订阅 IView:bindlayout(),createViewModel(),startPage(),finishPage(),createDatabinding(), showDialog(),disDialog(),showErro();
BaseModel:createRequestBody, changeObservable,mergeObservable;
BaseViewModel:create(),destroy()compossdisponse,createModel,LiveData
BaseActivity:Databinding绑定,VM对象创建,liveData关联,start()
SplashModel: 获取token SplashViewModel:token返回值获取，定时任务 SplashActivity:界面展示
RXFunction:RXJava操作符转换