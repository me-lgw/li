package com.bawei.common.field;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 10:48
 * Description: This is ActionEntity
 */
public class ActionEntity {
    public final static String STARTPAGE = "startPage";
    public final static String STARTBUNDLEPAGE = "startBundlePage";
    public final static String FINISHPAGE = "finishPage";
    public final static String SHOWDIALOG = "showdialog";
    public final static String DISDIALOG = "disdialog";
    public final static String SHOWERRO = "showerro";
    public final static String BUNDLEKEY = "bundlekey";
    public final static String CLASSKEY = "classkey";
    public final static String ERROKEY = "errokey";
}
