package com.bawei.common.bus;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 10:56
 * Description: This is LiveDataBus
 */

//依托原有LiveData处理掉多个观察者观察livedata时数据倒灌问题
public class LiveDataBus<T> extends MutableLiveData<T> {
    private AtomicBoolean flag = new AtomicBoolean(false);

    @Override
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        super.observe(owner, new Observer<T>() {
            @Override
            public void onChanged(T t) {
                //compareAndSet(boolean,boolean)目标值,更新值:原有值与目标值比对如相同更新boolean
                if (flag.compareAndSet(true,false)) {
                    observer.onChanged(t);
                }
            }
        });
    }

    @Override
    public void setValue(T value) {
        flag.set(true);//添加过数据
        super.setValue(value);
    }
}
