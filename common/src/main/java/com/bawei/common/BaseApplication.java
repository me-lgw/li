package com.bawei.common;

import android.app.Application;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 16:43
 * Description: This is BaseApplication
 */
public abstract class BaseApplication extends Application {
    private static BaseApplication instance;

    public static BaseApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
