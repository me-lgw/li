package com.bawei.common.network;

import com.bawei.common.field.URLConst;
import com.bawei.common.network.inteceptor.SignInterceptor;
import com.bawei.common.network.inteceptor.TokenInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 15:08
 * Description: This is TokenSignRetrofitImpl
 */
public class TokenSignRetrofitImpl implements RetrofitInterface {
    private Retrofit retrofit = null;
    @Override
    public Retrofit createRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(new SignInterceptor());
        builder.addInterceptor(new TokenInterceptor());
        builder.addInterceptor(loggingInterceptor);
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(30,TimeUnit.SECONDS);

        Retrofit.Builder retrofitbuilder = new Retrofit.Builder();
        retrofitbuilder.baseUrl(URLConst.BASEURL);
        retrofitbuilder.client(builder.build());
        retrofitbuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitbuilder.addConverterFactory(GsonConverterFactory.create());

        retrofit = retrofitbuilder.build();
        return retrofit;
    }
}
