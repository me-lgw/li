package com.bawei.common.network;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 15:06
 * Description: This is RetrofitFactory
 */
public class RetrofitFactory {
    private static SignRetrofitImpl signRetrofit;
    private static TokenRetrofitImpl tokenRetrofit;
    private static TokenSignRetrofitImpl tokenSignRetrofit;
    private static NomalRetrofitImpl nomalRetrofit;

    public static RetrofitInterface factory(HttpType type) {
        RetrofitInterface retrofitInterface = null;
        switch (type) {
            case SIGN:
                if (signRetrofit == null) {
                    signRetrofit = new SignRetrofitImpl();
                }
                retrofitInterface = signRetrofit;
                break;
            case TOKEN:
                if (tokenRetrofit == null) {
                    tokenRetrofit = new TokenRetrofitImpl();
                }
                retrofitInterface = tokenRetrofit;
                break;
            case TOKENSIGN:
                if (tokenSignRetrofit == null) {
                    tokenSignRetrofit = new TokenSignRetrofitImpl();
                }
                retrofitInterface = tokenSignRetrofit;
                break;
            default:
                if (nomalRetrofit == null) {
                    nomalRetrofit = new NomalRetrofitImpl();
                }
                retrofitInterface = nomalRetrofit;
        }
        return retrofitInterface;
    }
}
