package com.bawei.common.network.inteceptor;


import com.blankj.utilcode.util.SPUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 15:24
 * Description: This is TokenInterceptor
 */
public class TokenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        //获取原始接口请求得request->请求地址，请求方式，请求头，请求体
        Request oldRrequest = chain.request();

        //新建request->添加token使用
        Request.Builder builder = new Request.Builder();
        builder.url(oldRrequest.url());
        builder.headers(oldRrequest.headers());
        builder.addHeader("token", SPUtils.getInstance().getString("token"));
        builder.post(oldRrequest.body());
        return chain.proceed(builder.build());
    }
}
