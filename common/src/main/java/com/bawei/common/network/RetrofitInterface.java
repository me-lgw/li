package com.bawei.common.network;

import retrofit2.Retrofit;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 11:26
 * Description: This is RetrofitInterface
 */
public interface RetrofitInterface {
    Retrofit createRetrofit();
}
