package com.bawei.common.network;

import androidx.annotation.NonNull;

import com.bawei.common.entity.BaseEntity;

import io.reactivex.functions.Function;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 10:05
 * Description: This is RXFuntion
 */
public class RXFuntion<T extends BaseEntity,BaseEntity> implements Function<T, BaseEntity> {
    @Override
    public BaseEntity apply(@NonNull T t) throws Exception {
        return t;
    }
}
