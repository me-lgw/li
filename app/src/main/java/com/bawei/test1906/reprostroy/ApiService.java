package com.bawei.test1906.reprostroy;

import androidx.appcompat.app.AppCompatActivity;

import com.bawei.test1906.entity.TokenEtity;
import com.bawei.test1906.mallshop.BaseEntity;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/9
 * Time: 15:59
 * Description: This is ApiService
 */
public interface ApiService {
    @POST("sysToken/getToken")
    Observable<TokenEtity> requestToken(@Body RequestBody body);
}
