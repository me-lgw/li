package com.bawei.test1906.entity;

import com.bawei.common.entity.BaseEntity;
import com.google.gson.Gson;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 18:03
 * Description: This is TokenEtity
 */
public class TokenEtity extends BaseEntity {
    private String values;

    public void setValues(String values) {
        this.values = values;
    }

    public Values getVales() {
        Values value = new Gson().fromJson(values,Values.class);
        return value;
    }

    public static class Values{
        public String token;
    }
}
