package com.bawei.test1906.mvvm.view;

import com.bawei.common.mvvm.view.BaseActivity;
import com.bawei.test1906.BR;
import com.bawei.test1906.R;
import com.bawei.test1906.databinding.ActivitySplashBinding;
import com.bawei.test1906.mvvm.viewmodel.SplashViewModel;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 18:16
 * Description: This is SplashActivity
 */
public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> {

    @Override
    protected int createvarableId() {
        return BR.vm;
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_splash;
    }
}
