package com.bawei.test1906.mvvm.model;

import com.bawei.common.entity.BaseEntity;
import com.bawei.common.mvvm.model.BaseModel;
import com.bawei.common.network.HttpType;
import com.bawei.common.network.RetrofitFactory;
import com.bawei.test1906.entity.TokenEtity;
import com.bawei.test1906.reprostroy.ApiService;

import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 18:13
 * Description: This is SplashModel
 */
public class SplashModel extends BaseModel {

    public Observable<BaseEntity> requestToken(Map<String,Object> parames) {
        Observable<TokenEtity> observable = RetrofitFactory.factory(HttpType.SIGN)
                .createRetrofit().create(ApiService.class).requestToken(createBody(parames));

        return changeObservable(observable);
    }

}
