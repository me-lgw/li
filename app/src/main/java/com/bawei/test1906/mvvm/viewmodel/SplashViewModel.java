package com.bawei.test1906.mvvm.viewmodel;

import com.bawei.common.entity.BaseEntity;
import com.bawei.common.mvvm.viewmodel.BaseViewModel;
import com.bawei.test1906.entity.TokenEtity;
import com.bawei.test1906.mvvm.model.SplashModel;
import com.blankj.utilcode.util.LogUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2021/12/12
 * Time: 18:21
 * Description: This is SplashViewModel
 */
public class SplashViewModel extends BaseViewModel<SplashModel> {

    @Override
    protected void initData() {
        Map<String,Object> map = new HashMap<>();
        map.put("imie",""+System.currentTimeMillis());
        m.requestToken(map).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(this);
    }

    @Override
    protected void result(BaseEntity entity) {
        if (entity instanceof TokenEtity) {
            TokenEtity tokenEtity = (TokenEtity) entity;
            LogUtils.e(""+tokenEtity.message);
            LogUtils.e(""+tokenEtity.getVales().token);
        }
    }
}
